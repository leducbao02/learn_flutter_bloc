import 'package:bloc_http_flutter/models/Food_model.dart';
import 'package:bloc_http_flutter/services/api_food_sevice.dart';

class ApiRepository{
  final _apiFood = FoodApi();

  Future<List<FoodModel>> fetchListFood()async{
      var data = await _apiFood.fetchListFood();
      return data;
  }

}