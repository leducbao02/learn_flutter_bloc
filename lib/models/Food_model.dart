class FoodModel {
  FoodModel({
    required this.createdAt,
    required this.name,
    required this.image,
    required this.price,
    required this.description,
    required this.id,
  });

  FoodModel.fromJson(dynamic json) {
    createdAt = json['createdAt'];
    name = json['name'];
    image = json['image'];
    price = json['price'];
    description = json['description'];
    id = json['id'];
  }

  late final String createdAt;
  late final String name;
  late final String image;
  late final String price;
  late final String description;
  late final String id;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['createdAt'] = createdAt;
    map['name'] = name;
    map['image'] = image;
    map['price'] = price;
    map['description'] = description;
    map['id'] = id;
    return map;
  }
}
