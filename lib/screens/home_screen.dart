import 'package:bloc_http_flutter/bloc/bloc_food_bloc.dart';
import 'package:bloc_http_flutter/bloc/bloc_order/order_bloc.dart';
import 'package:bloc_http_flutter/models/Food_model.dart';
import 'package:bloc_http_flutter/screens/add_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // final FoodBloc _foodBloc = FoodBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // _foodBloc.add(Fet  chLisFood());
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Column(
        children: [
          Container(
            color: Colors.blueAccent,
            height: 100,
            child: const Center(
                child: Text(
              'HOME',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            )),
          ),
          BlocBuilder<FoodBloc,FoodState>(
              builder: (context, state){
               if(state is FoodLoading){
                   return Center(child: const CircularProgressIndicator());
               }
               if(state is FoodLoaded){
                 print(state.listFood.length);
                   return  Expanded(
                     child: ListView.builder(
                         itemCount:state.listFood.length,
                         // shrinkWrap: true,
                         itemBuilder: (context, index) => _itemHome(state.listFood,index,context)),
                   );

               }
               if(state is FoodLoadError){
                  return Text(state.messge);
               }
             return Container();
              },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (bulider) => AddScreen(),
            ),
          );
          // Navigator.of(context).pushNamed(AddScreen.routeName,);
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}


Widget _itemHome(List<FoodModel> foodModel, int Index,BuildContext context){
  return Card(
    child: ListTile(
      title: Wrap(
        children: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(10),
                child: SizedBox(
                  height: 90,
                  width: 63,
                  child: Image.network(foodModel[Index].image),
                ),
              ),
              Column(
                children: [
                  SizedBox(
                    width: 200,
                    child: Text(
                      overflow:TextOverflow.ellipsis,
                      foodModel[Index].name,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                  Container(
                      alignment: Alignment.topLeft,
                      margin: const EdgeInsets.fromLTRB(0, 10, 140, 0),
                      child: Text(
                        foodModel[Index].price,
                        style: const TextStyle(color: Colors.red),
                      )),
                ],
              ),
              InkWell(
                  onTap: () {

                    BlocProvider.of<OrderBloc>(context).add(OrderFoodEvent(foodModel[Index].id,foodModel[Index].name, foodModel[Index].image,foodModel[Index].price,foodModel[Index].description,1));
                    // print(widget.list[widget.Index].name);
                    ScaffoldMessenger.of(context).showSnackBar( const SnackBar(
                      content: Text("Thành công"),
                      duration: Duration(milliseconds: 500),
                    ));
                    // BuildContext.of<FoodBloc>(context).a
                    // bool check = false;
                    // widget.listNotifier.value.forEach((element) {
                    //   if (element.id == widget.item.id.toString()) {
                    //     check = true;
                    //   }
                    // });
                    // if (check) {
                    //   FoodModelOrder foodNew = widget.listNotifier.value[widget.index];
                    //   int quan = int.parse(foodNew.quantity ?? "1") + 1;
                    //   widget.listNotifier.value[widget.index] = foodNew.copyWith(quantity: quan.toString());
                    // } else {
                    //   FoodModels itemFood = widget.item;
                    //   FoodModelOrder newFood = FoodModelOrder(name: itemFood.name,image: itemFood.image,price: itemFood.price,createdAt: itemFood.createdAt,id: itemFood.id,description: itemFood.description,quantity: "1");
                    //   widget.listNotifier.value.add(newFood);
                    // }
                    // ScaffoldMessenger.of(context).showSnackBar( SnackBar(
                    //   content: Text("Thành công"),
                    //   duration: Duration(milliseconds: 500),
                    // ));



                  },
                  child: Container(
                    alignment: Alignment.topRight,
                    child: const Icon(
                      Icons.shopping_cart,
                      color: Colors.blue,
                    ),
                  ))
            ],
          ),
        ],

      ),
    ),
  );
}


