import 'package:bloc_http_flutter/bloc/bloc_food_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../widgets/text_input.dart';
class AddScreen extends StatefulWidget {
  const AddScreen({Key? key}) : super(key: key);

  @override
  State<AddScreen> createState() => _AddScreenState();
}

class _AddScreenState extends State<AddScreen> {
  late TextEditingController nameTextController;
  late TextEditingController priceTextController;
  late TextEditingController imageTextController;
  late TextEditingController descriptionTextController;

  @override
  void initState() {
    nameTextController = TextEditingController();
    priceTextController = TextEditingController();
    imageTextController = TextEditingController();
    descriptionTextController = TextEditingController();
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body:SingleChildScrollView(
          child: Column(
            children: [
              Container(
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(margin: EdgeInsets.fromLTRB(20, 0, 0, 0),child: GestureDetector(onTap: (){Navigator.of(context).pop();},child: Icon(Icons.arrow_back_ios,color: Colors.white,))),
                    Container(margin: EdgeInsets.fromLTRB(130, 0, 0, 0),child: Text('ADD',style: TextStyle(color: Colors.white,fontSize: 19,fontWeight: FontWeight.bold),)),
                  ],
                ),
                height: 90,
                color: Colors.blueAccent,
              ),
              Column(
                children: [
                  TextInput(textName: "Name", hintText: "Enter name", textEditingController: nameTextController),
                  TextInput(textName: "Price", hintText: "Enter price", textEditingController: priceTextController),
                  TextInput(textName: "Image", hintText: "Enter image link", textEditingController: imageTextController),
                  TextInput(textName: "Description", hintText: "Description", textEditingController: descriptionTextController)
                ],
              ),
              const SizedBox(height: 200,),
              Padding(
                padding: const EdgeInsets.fromLTRB(30,0,30,0),
                child: InkWell(
                  onTap: (){

                      add();
                    // if(name.isNotEmpty && price.isNotEmpty && image.isNotEmpty && des.isNotEmpty) {
                    //   http.Response  res = await foodService.addFood(name, image, price, des);
                    //   if(res.statusCode == 200){
                    //     print(res.body);
                    //   }else{
                    //     print("error");
                    //   }

                      // widget.list.add(FoodModels(name: name,description: des, price: price,image: image));

                      // print(widget.list.length);
                    },

                    // Navigator.of(context).pop();
                    // print(widget.list);
                  child: Container(
                    height: 55,
                    child: const Center(child: Text('SAVE',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),)),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.blue,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  add(){
    String name = nameTextController.text.trim();
    String price = priceTextController.text.trim().toString() ;
    String image = imageTextController.text.trim();
    String des = descriptionTextController.text.trim();
    BlocProvider.of<FoodBloc>(context).add(AddFood(name, price, image, des,context));
    print(name);
  }
}
