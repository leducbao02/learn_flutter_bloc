import 'package:bloc_http_flutter/screens/oder_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/bloc_food_bloc.dart';
import 'home_screen.dart';

class ScreenNav extends StatefulWidget {
  const ScreenNav({Key? key}) : super(key: key);

  @override
  State<ScreenNav> createState() => _ScreenNavState();
}

class _ScreenNavState extends State<ScreenNav> {

  int _currenIndex = 0;
  // FoodService foodService = FoodService();
  //  ValueNotifier<List<FoodModels> > listFood = ValueNotifier<List<FoodModels>>([]);
  // final ValueNotifier<List<FoodModelOrder>> listOderNotifier =  ValueNotifier<List<FoodModelOrder>>([]);
  // late List tab;

  @override
  void initState(){
    // loadData();

    // listFood = foodService.getApiFood();
    // TODO: implement initState
    super.initState();
    BlocProvider.of<FoodBloc>(context).add(FetchLisFood());
  }


  List tab = [
  HomeScreen(),
  OderScreen()
  // OderScreen(listOderNotifier:listOderNotifier)
  ];
  // void loadData()async{
  //   listFood =  await _getDataFood();
  //   tab = [
  //     HomeScreen(addItemOder:()=>{}, listOderNotifier: listOderNotifier,listFood: listFood),
  //     OderScreen(listOderNotifier:listOderNotifier)
  //   ];
  // }
  //
  // Future<ValueNotifier<List<FoodModels>>> _getDataFood() async{
  //   List<FoodModels> _listFood = await foodService.getApiFood();
  //   listFood =  ValueNotifier<List<FoodModels>>(_listFood);
  //   return listFood;
  // }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: tab[_currenIndex],
        bottomNavigationBar: BottomNavigationBar(
          items: const [
            BottomNavigationBarItem(icon: Icon(Icons.home),
                label: "Home"
            ),
            BottomNavigationBarItem(icon: Icon(Icons.border_all_rounded),
              label: "Order",

            ),
          ],
          currentIndex: _currenIndex,
          onTap: (value) {
            setState(() {
              _currenIndex = value;
            });
          },
        )
    );
  }
  }




