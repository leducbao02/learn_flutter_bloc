import 'package:bloc_http_flutter/bloc/bloc_order/order_bloc.dart';
import 'package:bloc_http_flutter/models/food_model_order.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
class DetailsScreen extends StatefulWidget {
  const DetailsScreen({Key? key, required this.item, required this.index}) : super(key: key);
  final List<FoodModelOrder> item ;
  final int index;

  @override
  State<DetailsScreen> createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: Colors.blueAccent,
              height: 100,
              child:  Center(child: Text(widget.item[widget.index].name.toString(),style: TextStyle(color:Colors.white, fontWeight: FontWeight.bold,fontSize: 20),)),
            ),

            Padding(padding: EdgeInsets.all(10),child:
            Image.network(widget.item[widget.index].image.toString()),),

            Container(
                margin: const EdgeInsets.fromLTRB(10, 0, 0, 10),
                child: Text(widget.item[widget.index].description.toString())),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: const Text("Price",style: TextStyle(fontWeight: FontWeight.bold),)),
                Container(
                    margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: Text(widget.item[widget.index].price.toString()))
              ],
            ),

            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: [
            //     Container(
            //         margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
            //         child: const Text("Total",style: TextStyle(fontWeight: FontWeight.bold))),
            //     Container(
            //         margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
            //         child: Text(totolPrice.toString())
            //     )
            //   ],
            // ),
            //
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: [
            //     Container(
            //         margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
            //         child: const Text("Quantity",style: TextStyle(fontWeight: FontWeight.bold))),
            //     Container(
            //         margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
            //         child:  Text(total.toString()))
            //
            //   ],
            // )


          ],
        ),
      ),
    );
  }
}
