part of 'bloc_food_bloc.dart';

abstract class FoodEvent extends Equatable {
  const FoodEvent();
}

class FetchLisFood extends FoodEvent{
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();

}

class AddFood extends FoodEvent{
 final String name, price, image,description;
 final BuildContext context;
  const AddFood(this.name,this.price,this.image,this.description,this.context);
  @override
  // TODO: implement props
  List<Object?> get props => [name,price,image,description,context];

}


class AddFoodOrder extends FoodEvent{
  final String id,name, price, image, description,quantity;
  const AddFoodOrder(this.id,this.name,this.price,this.image,this.description,this.quantity);

  @override
  // TODO: implement props
  List<Object?> get props => [id,name,price,image,description,quantity];

}



