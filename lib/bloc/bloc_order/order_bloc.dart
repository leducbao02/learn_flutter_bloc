import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bloc_http_flutter/bloc/bloc_food_bloc.dart';
import 'package:bloc_http_flutter/models/food_model_order.dart';
import 'package:bloc_http_flutter/screens/oder_screen.dart';
import 'package:equatable/equatable.dart';

import '../../services/api_repository.dart';

part 'order_event.dart';

part 'order_state.dart';

class OrderBloc extends Bloc<OrderEvent, OrderState> {
  List<FoodModelOrder> listOrder = [];

  OrderBloc() : super(OrderInitial()) {
    on<OrderEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<OrderFoodEvent>((event, emit) {
      try {
        // final list = await _apiRepository.fetchListFood();
        bool check = false;
        double totalPric = 0;
        int totalQuantity = 0;
        listOrder.forEach((element) {
          if (element.id == event.id) {
            check = true;
          }
        });
        print(check);

        if (check) {
            event.quantity + 1;
        } else {
          FoodModelOrder oder = FoodModelOrder(
              id: event.id,
              name: event.name,
              image: event.image,
              price: event.price,
              description: event.descrpiton,
              quantity: event.quantity.toString());
          listOrder.add(oder);
        }

        emit(OrderSuccess(listOrder));
      } catch (e) {
        emit(OderError("error"));
      }
    });
  }
}
